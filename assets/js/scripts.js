$(function () {
  $('.popover-name').popover({
    html: true,
    content: function(){
      return $('#popover-name').html();
    }
  });
  $('.popover-homepage').popover({
    html: true,
    content: function(){
      return $('#popover-homepage').html();
    }
  });
  $('.popover-phone').popover({
    html: true,
    content: function(){
      return $('#popover-phone').html();
    }
  });
  $('.popover-location').popover({
    html: true,
    content: function(){
      return $('#popover-location').html();
    }
  });
});

$('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

$(".btn-pencil").click(function() {
  $(".show-info").hide();
  $(".hidden-form").show();
});
$(".btn-action").click(function() {
  $(".hidden-form").hide();
  $(".show-info").show();
});
